<?php

namespace App;

//php artisan make:model Estado

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $fillable = ['descricao', 'uf'];

    public function cidades(){
        return $this->hasMany('App\Cidade', 'id_estado', 'id');
    }

}
