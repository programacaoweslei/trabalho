<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    protected $fillable = ['descricao'];

    public $timestamps = false;

    public function veiculo(){
        return $this->hasMany('App\Carro', 'id_carro', 'id');
    }
}
