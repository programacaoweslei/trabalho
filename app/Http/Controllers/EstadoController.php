<?php


//php artisan make:controller EstadoController --resource


namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Importar model Estado
use App\Estado;

class EstadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        //http://localhost:8000/api/estados?qtd=3
        $qtd = $request->input('qtd');
//        return Estado::all();

        return Estado::with('cidades')->paginate($qtd);

//        return response()->json( [
//            "request" => $request->all(),
////            "data" => Estado::paginate(2)
//            "dados" => Estado::paginate($request->input('limit'))
//        ],
//        200
//        );

//        $registros = DB::select("select cidade.descricao, estado.descricao from estado
//            inner join cidade
//            on estado.id_estado = cidade.id_estado");
//
//        return $registros;
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Inserir um registro na tabela estados
        $estado = new Estado();

        //$estado->descricao = $request->input('descricao');
        //$estado->uf = $request->input('uf');

        $estado->fill( $request->all() );

        $estado->save();

        return $estado;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        $estado = Estado::find( $id );
//        return $estado;



        $estado = Estado::with('cidades')->find( $id );

        return $estado;


    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $estado = Estado::find( $id );

        $estado->fill( $request->all() );
        //$estado->descricao = $request->input('descricao');
        $estado->save();
        //$estado->update( $request->all() );


        return $estado;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estado = Estado::find( $id );
        $estado->delete();
        return $estado;
    }

    public function teste(){
        return "Cidades de uma estado especifico";
    }

}
