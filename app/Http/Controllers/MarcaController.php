<?php

namespace App\Http\Controllers;

use Validator;
use Mockery\Exception;
use Illuminate\Http\Request;
use DB;

class MarcaController extends Controller
{

    private $atributos = ['descricao'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$marcas = DB::select('select * from marcas');

        try {

            return response()->json([DB::select('select * from marcas')->paginate(20)], 200);

        } catch (\Exception $exception) {

            return response()->json(["mensagem" => $exception->getMessage()], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {

            $validacao = $this->validar($request);

            if($validacao->fails()) {
                return response()->json([
                    'mensagem' => 'PAU',
                    'erros' => $validacao->errors()
                ], 400);
            }

            $marca = DB::insert("insert into marcas (descricao) values (?)", [$request->input("descricao")]);

            if ($marca) {
                return response()->json( [$marca], 201);
            } else {
                return response()->json(["mensagem" => "PARABENS JUVENAL"]);
            }

        } catch (\Exception $exception) {

            return response()->json(["mensagem" => $exception->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        try {

            if ($id > 0) {

                $marca = DB::select("select * from marcas where id = ?", [$id]);

                if ($marca) {
                    return $marca;
                } else {
                    return response()->json(["mensagem" => "Registro nao encontrado"], 404);
                }
            } else {
                return response()->json(["mensagem" => "ERRRRROU"], 400);
            }

        } catch (\Exception $exception) {

            return response()->json(["mensagem" => $exception->getMessage()], 500);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $marca = DB::update("update marcas set descricao = ? where id = ?", [$request->input("descricao"), $id]);

        return $marca;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::delete("delete from marcas where id = ?", [$id]);
    }

    public function validar($request)
    {
        $validator = Validator::make($request->only($this->atributos), [
            'descricao' => 'required|max:40'
        ]);

        return $validator;
    }
}
