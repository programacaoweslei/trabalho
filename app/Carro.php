<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carro extends Model
{
    protected $fillable = ['id_marca', 'descricao'];

    public $timestamps = false;

    public function marca(){
        return $this->belongsTo('App\Marca', 'id_marca', 'id');
    }
}
